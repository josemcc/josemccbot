/*globals require, process*/

/*-----------------------------------------------------------------------------
A simple Language Understanding (LUIS) bot for the Microsoft Bot Framework.
-----------------------------------------------------------------------------*/
// This loads the environment variables from the .env file
require('dotenv-extended').load();
const restify = require('restify');
const builder = require('botbuilder');
const azure = require('botbuilder-azure');
const log4js = require('log4js');
const logger = log4js.getLogger('APP:MAIN');
logger.level = 'debug';

// Intent handlers
// const SalesIntent = require('./intents/sales-intent');

const mongoApi = require('./lib/cosmosdb-api');

// Setup Restify Server
const server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
	logger.info('%s listening to %s', server.name, server.url);
});

// Create chat connector for communicating with the Bot Framework Service
const connector = new builder.ChatConnector({
	appId: process.env.MicrosoftAppId,
	appPassword: process.env.MicrosoftAppPassword,
	openIdMetadata: process.env.BotOpenIdMetadata
});

// Listen for messages from users
server.post('/api/messages', connector.listen());

/*----------------------------------------------------------------------------------------
* Bot Storage: This is a great spot to register the private state storage for your bot.
* We provide adapters for Azure Table, CosmosDb, SQL Azure, or you can implement your own!
* For samples and documentation, see: https://github.com/Microsoft/BotBuilder-Azure
// * ---------------------------------------------------------------------------------------- */
var inMemoryStorage = new builder.MemoryBotStorage();

// Create your bot with a function to receive messages from the user
// This default message handler is invoked if the user's utterance doesn't
// match any intents handled by other dialogs.
const bot = new builder.UniversalBot(connector, session => {
}).set('storage', inMemoryStorage);


// Make sure you add code to validate these fields
const luisAppId = process.env.LuisAppId;
const luisAPIKey = process.env.LuisAPIKey;
const luisStage = process.env.LuisStage || false;
const luisAPIHostName = process.env.LuisAPIHostName || 'westus.api.cognitive.microsoft.com';
const LuisModelUrl = 'https://' + luisAPIHostName + '/luis/v2.0/apps/' + luisAppId + '?subscription-key=' + luisAPIKey + `&staging=${luisStage}&verbose=true&timezoneOffset=0`;

// Create a recognizer that gets intents from LUIS, and add it to the bot
var recognizer = new builder.LuisRecognizer(LuisModelUrl);

// Register recognizer
bot.recognizer(recognizer);

// Middleware
bot.use({
	botbuilder: (session, next) => {
		try {
			const savedAddress = session.message.address;
			// Save this information somewhere that it can be accessed later, 
			// such as in a database, or session.userData
			session.userData.savedAddress = savedAddress;
			session.save();
			var message = session.message.text.toLowerCase();
				
			if(message.startsWith("read data")) {
				mongoApi.findStr(function(result) {
					var text = "";
					result.forEach(element => {
						text += JSON.stringify(element) + "\n";
					});
					session.send('Hello :) ');
					session.send(text);
				});
			} else if(message.startsWith("save data")) {
				try {
					json = JSON.parse(message.replace("save data", ""));
					
					mongoApi.writeStr(json, function(result) {
						session.send('Data saved');
					});
				} catch (error) {	
					session.send("Error al grabar: " + error.message);	
				}	
			} else {
				session.send('Hola');
			}	

		} catch (e) {
			logger.error(e);
		}
		next();
	}
});




// Sales Intents
// bot.dialog('SalesRealTimeDialog', SalesIntent.salesRealTimeController).triggerAction({ matches: 'Sales.Realtime' });
